//
//  DateError.swift
//  OpenMensa
//
//  Created by Franz Murner on 21.02.21.
//

import Foundation

enum DateError: Error{
    case wrongDateFormat
}
