//
//  DateManager.swift
//  OpenMensa
//
//  Created by Franz Murner on 21.02.21.
//

import UIKit

class DateManager{
    
    private static var singleton: DateManager?
    
    public static var shared: DateManager{
        get{
            if singleton == nil{
                singleton = DateManager().today()
            }
        
            return singleton!
        }
    }
    
    private init(){
        
    }
    
    
    
    private var currentDate = Date()
    private var oneDay = 24*60*60
    

    public func today()->DateManager{
        currentDate = Date()
        return self
    }
    
    public func withDate(dateString: String)throws ->DateManager{
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        guard let date = dateFormatter.date(from: dateString) else{
            throw DateError.wrongDateFormat
        }
        
        self.currentDate = date
        
        return self
        
    }
    
    public func withDate(date: Date)->DateManager{

        
        self.currentDate = date
        
        return self
        
    }
    
    
    
    public func perviousDay()->DateManager{
        currentDate.addTimeInterval(TimeInterval(-oneDay))
        return self
    }
    
    public func nextDay()->DateManager{
        currentDate.addTimeInterval(TimeInterval(oneDay))
        return self
    }
    
    func toString()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return "\(dateFormatter.string(from: currentDate))"
    }
    
    func getDate()->Date{
        return currentDate
    }
    
    func getDateManager()->DateManager{
        return self
    }
}
