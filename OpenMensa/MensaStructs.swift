//
//  MensaStructs.swift
//  OpenMensa
//
//  Created by Franz Murner on 18.02.21.
//

import Foundation

struct Mensa: Codable{
    let id: Int?
    let name, city, address: String?
    let coordinates: [Double]?
}

typealias MensaArray = [Mensa]

struct MensaOpen: Codable{
    let date: String?
    let closed: Bool?
}

typealias Meals = [Meal]

struct Meal: Codable{
    var id: Int?
    var name: String?
    var category: String?
    var prices: Prices?
    var notes: [String]?
    
}

struct Prices: Codable{
    var students: Float?
    var employees: Float?
    var pupils: Float?
    var others: Float?
}





