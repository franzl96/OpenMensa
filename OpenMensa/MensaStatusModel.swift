//
//  MensaStatusModel.swift
//  OpenMensa
//
//  Created by Franz Murner on 21.02.21.
//

import UIKit

class MensaStatusModel{
    
    private static var singleton: MensaStatusModel?
    
    public static var shared: MensaStatusModel{
        get{
            if singleton == nil{
                self.singleton = MensaStatusModel()
            }
            
            return singleton!
        }
    }
    
    private init(){
        
    }
    
    
    func isMensaClosed(mensa: Mensa, fromDate: DateManager, completion: @escaping (Bool)->()){
        
        guard let id = mensa.id else { return }
        
        let urlString = "https://openmensa.org/api/v2/canteens/\(id)/days/\(fromDate.toString())"
        guard let url = URL(string: urlString) else { return }
        
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil else {
                print(error?.localizedDescription ?? "Error")
                return
            }
            
            guard let response = response as? HTTPURLResponse, response.mimeType == "application/json" else {
                print("Wrong Mime Type")
                return
            }
            
            guard let data = data else { return }
            
            guard data.count != 0 else {
                DispatchQueue.main.async {
                    completion(true)
                }
                return
            }
            
            let jsonDec = JSONDecoder()
            
            do{
                let result = try jsonDec.decode(MensaOpen.self, from: data)
                
                guard let closed = result.closed else { return }
                DispatchQueue.main.async {
                    completion(closed)
                }
            }catch{
                print(error.localizedDescription)
            }
            
            
            
        
        }.resume()
        
    }
    
    
    
    
    
    
}
