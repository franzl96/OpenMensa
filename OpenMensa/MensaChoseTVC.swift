//
//  MensaChoseTVC.swift
//  OpenMensa
//
//  Created by Franz Murner on 19.02.21.
//

import UIKit

class MensaChoseTVC: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var reloadBtn: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navItem: UINavigationItem!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    let identifier = "Cell"
    
    var choosenMensa: Mensa?
    
    var mensen: [Mensa]?{
        didSet{
            mensenTable = mensen
        }
    }
    
    var mensenTable: [Mensa]?
    
    var searchText = ""

    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Mensa"
        
  
        
        navItem.searchController = searchController
        navItem.hidesSearchBarWhenScrolling = true
        
        definesPresentationContext = true
        
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: identifier)
        
        tableView.delegate = self
        tableView.dataSource = self

        activityIndicator.startAnimating()
        reloadBtn.isEnabled = false
        MensaModel.shared?.fetch(completion: { (mensen) in
            self.mensen = mensen
            self.tableView.reloadData()
            self.activityIndicator.stopAnimating()
            self.reloadBtn.isEnabled = true
        })
    }
    
   
    
    
    @IBAction func closeView(_ sender: Any){
        performSegue(withIdentifier: "goFromTwotoOne", sender: nil)
    }
    
    @IBAction func reloadView(_ sender: Any){
        activityIndicator.startAnimating()
        reloadBtn.isEnabled = false
        self.mensen = nil
        self.tableView.reloadData()
        MensaModel.shared?.reload(completion: { (mensen) in
            self.mensen = mensen
            self.filterAndSyncData()
            self.activityIndicator.stopAnimating()
            self.reloadBtn.isEnabled = true
        })
    }
    

}

extension MensaChoseTVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mensenTable?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        guard let mensen = mensenTable else {
            return cell
        }
        
        cell.textLabel?.text = "\((mensen[indexPath.row].id) ?? -1): \((mensen[indexPath.row].name) ?? "nil")"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let mensa = mensenTable?[indexPath.row] else { return }
        choosenMensa = mensa
        performSegue(withIdentifier: "goFromTwotoOne", sender: nil)
    }
}

extension MensaChoseTVC: UISearchResultsUpdating, UISearchBarDelegate{
    func updateSearchResults(for searchController: UISearchController) {
        guard let rawSearchText = searchController.searchBar.text else {return}
        searchText = rawSearchText
        filterAndSyncData()
    }
    
    
    private func filterAndSyncData(){
        guard let mensen = mensen else {
            return
        }
        
        guard searchText != "" else { return }
        
        mensenTable = mensen.filter({ (mensa) -> Bool in
            
            let result = mensa.name?.contains(searchText) ?? true ||
                (String(describing: mensa.id)).contains(searchText) ||
                mensa.city?.contains(searchText) ?? true ||
                mensa.address?.contains(searchText) ?? true
            
            return result
        })
        
        tableView.reloadData()
    }
    
    
}
