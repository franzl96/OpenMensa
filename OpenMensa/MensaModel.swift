//
//  MensaModel.swift
//  OpenMensa
//
//  Created by Franz Murner on 19.02.21.
//

import UIKit

class MensaModel{
    public var allMensas: [Mensa]!
    
    static private var singleton: MensaModel?
    
    static var shared: MensaModel?{
        get{
            if singleton == nil{
                singleton = MensaModel()
            }
            return singleton
        }
    }
    
    private init(){
        
    }
    
    public func reload(completion: @escaping (_ data: [Mensa]?)->Void){
        allMensas = [Mensa]()
        fetchAllMensas(count: 1) {
            completion(self.allMensas)
        }
    }
    
    public func fetch(completion: @escaping (_ data: [Mensa]?)->Void){
        if allMensas == nil{
            allMensas = [Mensa]()
            fetchAllMensas(count: 1) {
                completion(self.allMensas)
            }
        }else{
            completion(self.allMensas)
        }
    }
    
    private func fetchAllMensas(count : Int, completion: @escaping ()->Void){
                    
            
            let urlOpt = URL(string: "https://openmensa.org/api/v2/canteens?page=\(count)")
            
            
            guard let url = urlOpt else {return}
            
            
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard error == nil else {
                    print(error!.localizedDescription)
                    return
                }
                
                guard let response = response as? HTTPURLResponse, response.statusCode == 200 else{
                    print("No valid response Code!")
                    return
                }
                
                guard let data = data else {return}
                
                
                guard let mime = response.mimeType, mime == "application/json" else {
                    print("Wrong Mime Type!!")
                    return
                }
                
                var valOpt: MensaArray?
                
                do{
                
                valOpt = try JSONDecoder().decode(MensaArray.self, from: data)
                }catch{
                    print(error)
                }
                
                guard let val = valOpt, !(val.isEmpty) else {
                    DispatchQueue.main.async {
                        completion()
                    }
                    return
                }
                
                
                self.allMensas.append(contentsOf: val)
                
                self.fetchAllMensas(count: count + 1, completion: completion)
                
            }.resume()
    }
    
    
    func showResults(){
        DispatchQueue.main.async {
            for mensa in self.allMensas{
                print(mensa.name ?? "")
            }
        }
    }
}
