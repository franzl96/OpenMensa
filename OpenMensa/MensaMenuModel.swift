//
//  MensaMenuModel.swift
//  OpenMensa
//
//  Created by Franz Murner on 01.03.21.
//

import UIKit

class MensaMenuModel{
    
    private static var singleton: MensaMenuModel?
    public static var shared: MensaMenuModel{
        get{
            if singleton == nil{
                singleton = MensaMenuModel()
            }
            
            return singleton!
        }
    }
    
    private init(){}
    
    
    public func getMenuList(withID id: Int, fromDate: DateManager, completion: @escaping (Meals)->Void){
        let urlString = "https://openmensa.org/api/v2/canteens/\(id)/days/\(fromDate.toString())/meals"
        
        guard let url = URL(string: urlString) else{
            print("URL Error: MensaMenuModel")
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil else{
                print(error?.localizedDescription ?? "")
                return
            }
            guard let data = data else { return }
               
            guard let response = response as? HTTPURLResponse, response.mimeType == "application/json" else {
                
                print("Wrong Mime Type")
                return
            }
            
            let jsonDec = JSONDecoder()
            
            do{
                let result = try jsonDec.decode(Meals.self, from: data)
            
                DispatchQueue.main.async {
                    completion(result)
                }
                
                
            }catch{
                print(error.localizedDescription)
            }
            
        
            
        
        }.resume()
        
        
        
        
        

    }
    
    
    
    
    
    
    
    
    
}
