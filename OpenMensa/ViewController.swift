//
//  ViewController.swift
//  OpenMensa
//
//  Created by Franz Murner on 18.02.21.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var mensaInformation: UITextView!
    @IBOutlet weak var openLBL: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var mealOutput: UITableView!
    @IBOutlet weak var statusLabel: UILabel!
    
    var choosenMensa: Mensa?
    
    var meals = [Meal]()
    
    let cellId = "Cell"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        mealOutput.delegate = self
        mealOutput.dataSource = self
        mealOutput.isHidden = true
        
        mealOutput.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        statusLabel.isHidden = false
        statusLabel.text = "Choose Mensa!"


        MensaModel.shared?.fetch(completion: { (_) in print("Mensen initialized!")
            
        })
        
        
        
    }
    
    func getMensaData(){
        
        guard let choosenMensa = choosenMensa else {
            mealOutput.isHidden = true
            statusLabel.isHidden = false
            statusLabel.text = "Choose Mensa!"
            return
        }
        
        MensaStatusModel.shared.isMensaClosed(mensa: choosenMensa, fromDate: DateManager.shared) { (result) in
            
            guard result == false else {
                self.meals.removeAll()
                self.mealOutput.reloadData()
                self.mealOutput.isHidden = true
                
                self.statusLabel.isHidden = false
                self.statusLabel.text = "Closed"
                
                return
            }
            
            guard let id = choosenMensa.id else { return }
            
            MensaMenuModel.shared.getMenuList(withID: id, fromDate: DateManager.shared) { (meals) in
                self.meals = meals
                self.mealOutput.isHidden = false
                self.statusLabel.isHidden = true
                self.mealOutput.reloadData()
            }
            
            
            
        }
    }
    

    
    @IBAction func goToViewOne(_ segue: UIStoryboardSegue){
        guard let segue = segue.source as? MensaChoseTVC else { return }
        let name = "\(segue.choosenMensa?.name ?? "")\n"
        let address = "\(segue.choosenMensa?.address ?? "")"
        let streetandcity = address.split(separator: ",")
        
        guard streetandcity.count == 2 else { return }
        
        let street = "\(streetandcity[0])\n"
        let city = "\(streetandcity[1])"
        
        self.choosenMensa = segue.choosenMensa
        
        mensaInformation.text = "\(name)\(street)\(city)"
        
        getMensaData()

    }
    
    @IBAction func nextDay(_ sender: Any) {
        datePicker.date = DateManager.shared.nextDay().getDate()
        getMensaData()
    }
    
    @IBAction func prevDay(_ sender: Any) {
        datePicker.date = DateManager.shared.perviousDay().getDate()
        getMensaData()
    }
    
            
    @IBAction func openDataPicker(_ sender: Any) {
        _ = DateManager.shared.withDate(date: datePicker.date)
        getMensaData()
    }
    
}

extension ViewController{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: cellId)
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.numberOfLines = 5
        cell.textLabel?.text = meals[indexPath.row].name ?? ""
        return cell
    }
    
}


